using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GoalCount : MonoBehaviour
{
    public TMP_Text goalCount;
    public GameObject player;
    int count = 0;

    float x, y, z;

    public void Awake()
    {
         x = gameObject.transform.position.x;
         y = gameObject.transform.position.y;
         z = gameObject.transform.position.z;
    }


    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("post"))
        {
            count++;
            gameObject.transform.position = new Vector3(x, y, z);
            player.transform.position = new Vector3(GameControllerScript.x_pos, GameControllerScript.y_pos, GameControllerScript.z_pos);

        }
        goalCount.text = "Goal :" + " " + count;
    }
}
